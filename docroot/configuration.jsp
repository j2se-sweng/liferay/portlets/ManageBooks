<%@ include file="/init.jsp" %>
<%@page import="com.liferay.portal.kernel.util.Constants" %>

<liferay-portlet:actionURL portletConfiguration="true" var="configurationActionURL" />
<liferay-portlet:renderURL portletConfiguration="true" varImpl="configurationRenderURL" />

<aui:form action="<%= configurationActionURL %>" method="post" name="fm" >
	<aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= Constants.UPDATE %>" />
	<aui:input name="redirect" type="hidden" value="<%= configurationRenderURL.toString() %>" />

	<aui:input label="show-pagination" name="preferences--showPagination--" type="checkbox" value="<%= showPagination %>" />

	<aui:button-row>
		<aui:button type="submit" />
	</aui:button-row>

</aui:form>