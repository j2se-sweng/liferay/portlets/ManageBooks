package it.unisef.managebooks.hook.listeners;

import it.unisef.managebooks.service.BookLocalServiceUtil;

import com.liferay.portal.ModelListenerException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.BaseModelListener;
import com.liferay.portal.model.Group;

public class GroupListener extends BaseModelListener<Group> {

	@Override
	public void onBeforeRemove(Group model) throws ModelListenerException {

		try {
			BookLocalServiceUtil.deleteGroupBooks(model.getGroupId());

			_log.info("Titoli del sito " + model.getGroupId() + " cancellati.");
		}
		catch (Exception e) {
			_log.info("Errore durante la cancellazione dei Titoli del gruppo/sito " +
						model.getGroupId() );
		}

	};


	private static Log _log = LogFactoryUtil.getLog(GroupListener.class);

}

