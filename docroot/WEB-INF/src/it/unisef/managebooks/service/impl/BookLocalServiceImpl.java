/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.unisef.managebooks.service.impl;

import it.unisef.managebooks.BookAuthorException;
import it.unisef.managebooks.BookPagesException;
import it.unisef.managebooks.BookPriceException;
import it.unisef.managebooks.BookTitleException;
import it.unisef.managebooks.model.Book;
import it.unisef.managebooks.service.base.BookLocalServiceBaseImpl;

import java.util.Date;
import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ResourceConstants;
import com.liferay.portal.model.User;
import com.liferay.portal.service.ServiceContext;

/**
 * The implementation of the book local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.unisef.managebooks.service.BookLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Giulio Piemontese
 * @see it.unisef.managebooks.service.base.BookLocalServiceBaseImpl
 * @see it.unisef.managebooks.service.BookLocalServiceUtil
 */
public class BookLocalServiceImpl extends BookLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.unisef.managebooks.service.BookLocalServiceUtil} to access the book local service.
	 */

	public Book addBook(
			long userId, String title, String author,
			String description,
			String publisher,
			int pageNumber,
			double price,
			ServiceContext serviceContext)
				throws PortalException, SystemException {

		User user = userPersistence.findByPrimaryKey(userId);
		long groupId = serviceContext.getScopeGroupId();


		validate(title, author);

		validate2(pageNumber, price);

		long bookId = counterLocalService.increment();

		// Creazione del Bean

		Book book = bookPersistence.create(bookId);

		book.setUserId(user.getUserId());
		book.setUserName(user.getFullName());
		book.setGroupId(groupId);
		book.setCompanyId(user.getCompanyId());

		Date now = new Date();
		book.setCreateDate(serviceContext.getCreateDate(now));
		book.setModifiedDate(serviceContext.getModifiedDate(now));

		book.setTitle(title);
		book.setAuthor(author);
		book.setDescription(description);
		book.setPublisher(publisher);
		book.setPages(pageNumber);
		book.setPrice(price);

		// La bookPersistence pu� essere solo richiamata dalla LocalServiceImpl
		book = bookPersistence.update(book, false); // false
													// non fa il merge dei dati che eventualmente gi� ci sono

		return book;
	}

	public Book getBook(long bookId)
			throws PortalException, SystemException {

		//_log.info("[local] BookService::getBook()");

		return bookPersistence.findByPrimaryKey(bookId);
	}

	public List<Book> getBooks(long companyId, long groupId, int start, int end)
			throws SystemException {

			return bookPersistence.findByC_G(companyId, groupId, start, end);
	}

	public List<Book> getBooks(long userId, long companyId, long groupId,
			int start, int end)
			throws SystemException {

			return bookPersistence.findByU_C_G(userId, companyId, groupId,
					start, end);
	}

	public int getBooksCount(long companyId, long groupId)
			throws SystemException {

		return bookPersistence.countByC_G(companyId, groupId);
	}

	public int getBooksCount(long userId, long companyId, long groupId)
			throws SystemException {

		return bookPersistence.countByU_C_G(userId, companyId, groupId);
	}

	public Book updateBook(
			long bookId, String title, String author,
			String description,
			String publisher,
			int pageNumber,
			double price
			/*, ServiceContext context , nel caso in cui la portlet serva a
			 * mostrare degli asset */)

		throws PortalException, SystemException {

		Book book = bookPersistence.findByPrimaryKey(bookId);

		validate(title, author);

		validate2(pageNumber, price);

		book.setModifiedDate(new Date());

//		Calendar expirationDate = CalendarFactoryUtil.getCalendar();
//
//		expirationDate.set(Calendar.MONTH, expirationDateMonth);
//		expirationDate.set(Calendar.DAY_OF_MONTH,  expirationDateDay);
//		expirationDate.set(Calendar.YEAR, expirationDateYear);
//		expirationDate.set(Calendar.HOUR_OF_DAY, 0);
//		expirationDate.set(Calendar.MINUTE, 0);
//		expirationDate.set(Calendar.SECOND, 0);
//
//		book.setExpirationDate(expirationDate.getTime());

		book.setTitle(title);
		book.setAuthor(author);
		book.setDescription(description);
		book.setPublisher(publisher);

		book.setPages(pageNumber);
		book.setPrice(price);

		book = bookPersistence.update(book, false);

		return book;

	}

	@Override
	public Book deleteBook(Book book)
			throws SystemException, PortalException {

		bookPersistence.remove(book);

		resourceLocalService.deleteResource(
				book.getCompanyId(),
				Book.class.getName(),
				ResourceConstants.SCOPE_INDIVIDUAL,
				book.getBookId());

		return book;
	}

	public void deleteUserBooks(long userId) {

		try {

			List<Book> books = bookPersistence.findByUser(userId);

			for(Book t : books) {
				bookPersistence.remove(t);
			}
		}

		catch (Exception e){
			e.printStackTrace();
		}
	}

	public void deleteGroupBooks(long groupId) {
		try {

			List<Book> books = bookPersistence.findByGroup(groupId);

			for(Book t : books) {
				bookPersistence.remove(t);
			}
		}

		catch (Exception e){
			e.printStackTrace();
		}
	}


	public void deleteBooks(long userId, long groupId) {
		try {

			List<Book> books = bookPersistence.findByU_G(groupId, userId);

			for(Book t : books) {
				bookPersistence.remove(t);
			}
		}

		catch (Exception e){
			e.printStackTrace();
		}
	}


	public void validate(String title, String author)
		throws BookTitleException, BookAuthorException {

		if(Validator.isNull(title)) {
			throw new BookTitleException();
		}

		if(Validator.isNull(author)) {
			throw new BookAuthorException();
		}


	}

	public void validate2(int pages, double price)
			throws BookPagesException, BookPriceException {

			if(pages <= 0) {
				throw new BookPagesException();
			}

			if(price < 0) {
				throw new BookPriceException();
			}


	}

}