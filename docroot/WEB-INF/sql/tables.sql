create table MB_Book (
	bookId LONG not null primary key,
	companyId LONG,
	userId LONG,
	groupId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	title VARCHAR(75) null,
	author VARCHAR(75) null,
	description STRING null,
	publisher VARCHAR(75) null,
	pages INTEGER,
	price DOUBLE
);